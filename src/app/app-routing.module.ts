import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ChucNangCon1Component} from './chuc-nang-con1/chuc-nang-con1.component';
import {IndexLayoutComponent} from './layout1/index-layout/index-layout.component';
import {IndexLayout2Component} from './layout2/index-layout2/index-layout2.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {DanhMucGuardService} from './danh-muc/danh-muc-guard.service';
import {DangNhapComponent} from './dang-nhap/dang-nhap.component';

const routes: Routes = [
  {
    path: 'layout1',
    component: IndexLayoutComponent,
    children: [
      {
        path: 'chuc-nang-con-1',
        component: ChucNangCon1Component
      },
      {
        canActivate: [DanhMucGuardService],
        path: 'danh-muc',
        loadChildren: () => import('./danh-muc/danh-muc.module').then((m) => m.DanhMucModule),
      },
      {
        path: 'dang-nhap',
        component: DangNhapComponent
      },
      {
        path: '',
        redirectTo: 'dang-nhap',
        pathMatch: 'full'
      },
      {
        path: '**',
        component: PageNotFoundComponent
      }
    ]
  },
  {
    path: 'layout2',
    component: IndexLayout2Component,
    children: [
      {
        path: 'chuc-nang-con-1',
        component: ChucNangCon1Component
      }
    ]
  },
  {
    path: '',
    redirectTo: 'layout1',
    pathMatch: 'full'
  },
  {
    path: 'chuc-nang-con-1',
    component: ChucNangCon1Component
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [DanhMucGuardService]
})
export class AppRoutingModule {
}
