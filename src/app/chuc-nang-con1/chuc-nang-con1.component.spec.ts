import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChucNangCon1Component } from './chuc-nang-con1.component';

describe('ChucNangCon1Component', () => {
  let component: ChucNangCon1Component;
  let fixture: ComponentFixture<ChucNangCon1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChucNangCon1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChucNangCon1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
