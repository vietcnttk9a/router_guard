import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Observable, of} from 'rxjs';
import {switchMap} from 'rxjs/operators';

interface IdataTinh {
  ten: string;
  huyenId: number;
}

@Component({
  selector: 'app-sua-tinh',
  templateUrl: './sua-tinh.component.html',
  styleUrls: ['./sua-tinh.component.css']
})
export class SuaTinhComponent implements OnInit {
  dataTinh: IdataTinh;

  constructor(private route: ActivatedRoute) {
    this.route.params.pipe(switchMap((param) => {
      const tinhId = param.tinhId;
      return this.getData(tinhId);
    })).subscribe(res => {
      this.dataTinh = res;
    });
  }

  ngOnInit(): void {
  }

  private getData(tinhId: number): Observable<IdataTinh> {
    return of({ten: 'Bắc giang', huyenId: 1000});
  }


}
