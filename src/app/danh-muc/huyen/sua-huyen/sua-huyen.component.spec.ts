import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuaHuyenComponent } from './sua-huyen.component';

describe('SuaHuyenComponent', () => {
  let component: SuaHuyenComponent;
  let fixture: ComponentFixture<SuaHuyenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuaHuyenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuaHuyenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
