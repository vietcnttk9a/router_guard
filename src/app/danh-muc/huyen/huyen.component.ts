import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-huyen',
  templateUrl: './huyen.component.html',
  styleUrls: ['./huyen.component.css']
})
export class HuyenComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  suaHuyen() {
    this.router.navigate(['layout1/danh-muc/sua-huyen'], {queryParams: {huyenId: 11}});
  }
}
