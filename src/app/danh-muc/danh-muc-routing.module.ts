import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TinhComponent} from './tinh/tinh.component';
import {LayoutDanhMucComponent} from './layout-danh-muc/layout-danh-muc.component';
import {HuyenComponent} from './huyen/huyen.component';
import {SuaHuyenComponent} from './huyen/sua-huyen/sua-huyen.component';
import {SuaTinhComponent} from './tinh/sua-tinh/sua-tinh.component';
import {XaComponent} from './xa/xa.component';
import {ChiTietComponent} from './xa/chi-tiet/chi-tiet.component';
import {SuaXaComponent} from './xa/sua-xa/sua-xa.component';
import {ThemXaComponent} from './xa/them-xa/them-xa.component';
import {DanhMucGuardService} from './danh-muc-guard.service';

const routes: Routes = [
  {
    path: '',
    component: LayoutDanhMucComponent,
    canActivateChild: [DanhMucGuardService],
    children: [
      {
        component: TinhComponent,
        path: 'tinh'
      },
      {
        component: SuaTinhComponent,
        path: 'tinh/:tinhId'
      },
      {
        component: HuyenComponent,
        path: 'huyen'
      },
      {
        component: SuaHuyenComponent,
        path: 'sua-huyen'
      },
      {
        component: XaComponent,
        path: 'xa',
        children: [
          {
            component: SuaXaComponent,
            path: 'sua-xa',
            outlet: 'right'
          },
          {
            outlet: 'right',
            component: ThemXaComponent,
            path: 'them-xa'
          },
          {
            component: ThemXaComponent,
            path: 'them-xa'
          },
          {
            outlet: 'bottom',
            component: ChiTietComponent,
            path: 'chi-tiet'
          },
        ]
      }

    ]
  },
  {
    redirectTo: 'tinh',
    path: '',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DanhMucRoutingModule {
}
