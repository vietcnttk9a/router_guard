import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DanhMucRoutingModule } from './danh-muc-routing.module';
import { TinhComponent } from './tinh/tinh.component';
import { LayoutDanhMucComponent } from './layout-danh-muc/layout-danh-muc.component';
import { HuyenComponent } from './huyen/huyen.component';
import {SuaHuyenComponent} from './huyen/sua-huyen/sua-huyen.component';
import { SuaTinhComponent } from './tinh/sua-tinh/sua-tinh.component';
import {NzTableModule} from 'ng-zorro-antd/table';
import {NzGridModule} from 'ng-zorro-antd/grid';
import {XaComponent} from './xa/xa.component';
import { SuaXaComponent } from './xa/sua-xa/sua-xa.component';
import { ChiTietComponent } from './xa/chi-tiet/chi-tiet.component';
import { ThemXaComponent } from './xa/them-xa/them-xa.component';


@NgModule({
  declarations: [
    TinhComponent,
    LayoutDanhMucComponent,
    HuyenComponent,
    SuaHuyenComponent,
    SuaTinhComponent,
    XaComponent,
    SuaXaComponent,
    ChiTietComponent,
    ThemXaComponent

  ],
  imports: [
    CommonModule,
    DanhMucRoutingModule,
    NzTableModule,
    NzGridModule
  ]
})
export class DanhMucModule { }
