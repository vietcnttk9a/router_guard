import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuaXaComponent } from './sua-xa.component';

describe('SuaXaComponent', () => {
  let component: SuaXaComponent;
  let fixture: ComponentFixture<SuaXaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuaXaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuaXaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
