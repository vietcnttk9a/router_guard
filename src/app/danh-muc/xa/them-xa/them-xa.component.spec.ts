import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThemXaComponent } from './them-xa.component';

describe('ThemXaComponent', () => {
  let component: ThemXaComponent;
  let fixture: ComponentFixture<ThemXaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThemXaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemXaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
