import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

interface Person {
  key: string;
  name: string;
  age: number;
  address: string;
}

@Component({
  templateUrl: './xa.component.html',
  styleUrls: ['./xa.component.css']
})
export class XaComponent implements OnInit {
  listOfData: Person[] = [
    {
      key: '1',
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park'
    },
    {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park'
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park'
    }
  ];

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  suaData(data: Person) {
    this.router.navigate(['layout1/danh-muc/xa', {outlets: {right: 'sua-xa'}}]);
    // this.router.navigate(['layout1/danh-muc/xa/sua-xa']);
  }

  chiTiet(data: any) {
    this.router.navigate(['layout1/danh-muc/xa', {outlets: {bottom: 'chi-tiet'}}]);
  }
}
