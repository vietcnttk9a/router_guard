import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {NZ_I18N} from 'ng-zorro-antd/i18n';
import {en_US} from 'ng-zorro-antd/i18n';
import {registerLocaleData} from '@angular/common';
import en from '@angular/common/locales/en';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NzLayoutModule} from 'ng-zorro-antd/layout';
import {NzBreadCrumbModule} from 'ng-zorro-antd/breadcrumb';
import {NzMenuModule} from 'ng-zorro-antd/menu';
import {ChucNangCon1Component} from './chuc-nang-con1/chuc-nang-con1.component';
import {AppRoutingModule} from './app-routing.module';
import {Layout1Module} from './layout1/layout1.module';
import {Layout2Module} from './layout2/layout2.module';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {IconDefinition} from '@ant-design/icons-angular';
import * as AllIcons from '@ant-design/icons-angular/icons';

// Import what you need. RECOMMENDED. ✔️
import {AccountBookFill, AlertFill, AlertOutline} from '@ant-design/icons-angular/icons';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { DangNhapComponent } from './dang-nhap/dang-nhap.component';

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map((key) => antDesignIcons[key]);
registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    ChucNangCon1Component,
    PageNotFoundComponent,
    DangNhapComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzLayoutModule,
    NzBreadCrumbModule,
    NzMenuModule,
    AppRoutingModule,
    Layout1Module,
    Layout2Module,
    NzIconModule.forRoot(icons),
  ],
  providers: [{provide: NZ_I18N, useValue: en_US}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
