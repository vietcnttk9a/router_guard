import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IndexLayout2Component} from './index-layout2/index-layout2.component';
import {NzLayoutModule} from 'ng-zorro-antd/layout';
import {NzMenuModule} from 'ng-zorro-antd/menu';
import {NzBreadCrumbModule} from 'ng-zorro-antd/breadcrumb';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {RouterModule} from '@angular/router';


@NgModule({
  declarations: [
    IndexLayout2Component
  ],
  exports: [
    IndexLayout2Component
  ],
  imports: [
    CommonModule,
    NzLayoutModule,
    NzMenuModule,
    NzBreadCrumbModule,
    NzIconModule,
    RouterModule
  ]
})
export class Layout2Module {
}
